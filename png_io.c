#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <png.h>

int read_png3(char* filename, int* width, int* height, unsigned char** rgb) {

    FILE* infile = fopen(filename, "rb");
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytepp row_pointers;
    int bit_depth, color_type, interlace_type;

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
				   (png_voidp) NULL,
				   (png_error_ptr) NULL,
				   (png_error_ptr) NULL);

    info_ptr = png_create_info_struct(png_ptr);
    
    if (setjmp(png_jmpbuf(png_ptr)))
    {
      png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
      fclose(infile);
      return(0);
    }

    png_init_io(png_ptr, infile);
    png_read_update_info(png_ptr, info_ptr);

    png_get_IHDR(png_ptr, info_ptr, width, height, &bit_depth, &color_type,
	       &interlace_type, (int *) NULL, (int *) NULL);

    
    row_pointers = malloc(*height * sizeof(png_bytep));
    if (row_pointers == NULL)
    {
        fprintf(stderr, "Can't allocate memory for PNG file.\n");
        return(0);
    }

    for (i = 0; i < *height; i++)
    {
        row_pointers[i] = malloc(4 * *width);
        if (row_pointers == NULL)
            {
	            fprintf(stderr, "Can't allocate memory for PNG line.\n");
	            return(0);
        }
    }




}
