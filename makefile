cudaImageProcessing: cudaImageProcessing.cu
	nvcc -w -Xptxas -v,-O3 -I. -g -O3 -o processing cudaImageProcessing.cu png_util.c -lpng -lm -Xcompiler -fopenmp

clean:
	rm processing
      
